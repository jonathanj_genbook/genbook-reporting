# Genbook Reporting Scripts

Location for scripts used to do periodic reporting.

SQL scripts should be run in the following way:

- psql -h <host> -f DailyCancellationFeedbackDumpToCSV.sql -U genbook genbook
- psql -h <host> -f DailyYearToDateSPList.sql -U genbook genbook
- psql -h <host> -f DailyLifecycleStatus.sql -U genbook genbook
- psql -h <host> -f DailyLifecycleDelta.sql -U genbook genbook
- psql -h <host> -f DailySPAddressReview.sql -U genbook genbook
- psql -h <host> -f MonthlyChurn.sql -U genbook genbook
- psql -h <host> -f DailyNewPayers.sql -U genbook genbook
- psql -h <host> -f DailyDeactivation.sql -U genbook genbook
